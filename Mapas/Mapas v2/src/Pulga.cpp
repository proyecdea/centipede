#include "Pulga.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include "windows.h"
#include "mmsystem.h"
#include "Arania.h"
#include "Hongo.h"
#include "Sonidos.h"

using namespace std;

Pulga::Pulga(int anchura,int altura, int rapidez,int vidas, int puntaje){
    x=anchura;
    y=altura;
    velocidad=rapidez;
    vida=vidas;
    puntos=puntaje;
}

Pulga::Pulga(){

}

void Pulga::color(int X){
SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),X);
}

void Pulga::gotoxy(int x, int y){
	HANDLE hCon;
	hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
	dwPos.X = x;
	dwPos.Y = y;
	SetConsoleCursorPosition(hCon, dwPos);
}

int Pulga::getx(){
    return x;
}

void Pulga::setx(int anchura) {
    x = anchura;
}

int Pulga::gety() {
    return y;
}

void Pulga::sety(int altura) {
    y = altura;
}

int Pulga::getvelocidad() {
    return velocidad;
}

void Pulga::setvelocidad(int rapidez) {
    velocidad = rapidez;
}

int Pulga::getvida() {
    return vida;
}

void Pulga::setvida(int vidas) {
    vida = vidas;
}

int Pulga::getpuntos() {
    return puntos;
}

void Pulga::setpuntos(int puntaje) {
    puntos = puntaje;
}

int Pulga::printpulga(int y, int n, Hongo hongos[170], int colision){
    int in=0;
    //Validacion de que aun no ha sido disparada
    if (colision == 2)
    {
        in=0;
        setx(0);
        sety(0);
        setvida(0);
    }
    else
    {
        if (n>startpulga+1500 and getvida()>0)
        {
            startpulga = n + 1;
            if (xpulga + 5 < 32)
            {
                xpulga = xpulga + 5;
            }
        }
    }
    //impresion de pulga
    color(6);
    int x = xpulga;
    int w = startpulga;
    int w2 = 20;
    int z;
    if (vida > 0)
    {
        for (int y = 3; y<=34; y++)
        {
            w = w + 20;
            w2 = w + 20;
            z = w2 + 20;
            if (n>w and n<=w2 and y != 33)
            {
                in=1;
                this->x = x;
                this->y = y;
                if (y != 34)
                {
                    gotoxy(x, y);
                    printf("%c",42);
                }
            }
            if (n>w2 and n<=z and y != 33)
            {
                this->x = x;
                this->y = y;
                gotoxy(x, y);
                cout<<" ";
            }
            if (this->y == 33)
            {
                this->y = 0;
                this->x = 0;
            }
            for (int i=0; i<170; i++)
            {
                if(hongos[i].getx()==getx() && hongos[i].gety()==(gety()-1))
                {
                        hongos[i].printhongo();
                }
                if(hongos[i].getestado() == 5) break;
            }
            color(6);
        }
    }
    else
    {
        gotoxy(x,y);
        cout<<" ";
        this->x = 0;
        this->y = 0;
    }
    return in;
}

void Pulga::Start(int pulgastart, int pulgax){
    startpulga = pulgastart;
    xpulga = pulgax;
}

void Pulga::Restart(int pulgastart, int pulgax){
    gotoxy(x,y);
    cout<<" ";
    x = 0;
    y = 0;
    vida = 1;
    startpulga = pulgastart + 200;
    if (pulgax > 15)
    {
        xpulga = pulgax -10;
    }
    if (pulgax + 10 < 30)
    {
        xpulga = pulgax +10;
    }
}
