#include "Arania.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include "windows.h"
#include "mmsystem.h"
#include "Hongo.h"
#include "Sonidos.h"

using namespace std;

Arania::Arania(int y, int velocidad, int vida, int puntaje)
{
    this->y = y;
    this->velocidad = velocidad;
    this->vida = vida;
    this->puntaje = puntaje;
}

Arania::Arania()
{

}

void Arania::color(int X)
{
SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),X);
}

void Arania::gotoxy(int x, int y){
	HANDLE hCon;
	hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
	dwPos.X = x;
	dwPos.Y = y;
	SetConsoleCursorPosition(hCon, dwPos);
}


void Arania::printarana()
{
    printf("%c", 201);
    printf("%c", 153);
    printf("%c", 187);
}


int Arania::PrintArania(int velocidad, int n, Hongo hongos[170], int colision)
{
    int in=0;
    //Validacion de que aun no ha sido disparada
    if (colision == 3)
    {
        in=0;
        setx(0);
        sety(0);
        setvida(0);
    }
    else
    {
        if (n>startarana+1500 and getvida()>0)
        {
            startarana = n + 1;
            if (xarana + 5 < 34)
            {
                xarana = xarana + 5;
            }
        }
    }
    //Impresion de ara�a
    color(8);
    if (vida > 0)
    {
        int z = 0;
        int v = velocidad;
        int w = 0;
        int w2 = 10;
        int off = 0;
        int ex = 1;
        int finish = 0;
        int posy = xarana;
        if (n>=startarana and n <= startarana+300)
        {
            in=1;

            for (x=1; x<31; x++)
            {
                w = 0;
                w2 = 10;
                if (x<=4)
                {
                    if (x==1)
                    {
                        z = 1;
                    }
                    z++;
                    y = posy-z;

                    if (n>startarana+0 and n<startarana+10 and x ==1)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+10 and x == 1)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+10 and n<startarana+20 and x ==2)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+20 and x == 2)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+20 and n<startarana+30 and x ==3)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+30 and x == 3)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+30 and n<startarana+40 and x ==4)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+40 and x == 4)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                }
                if (x>4 and x<=8)
                {
                    if (x==5)
                    {
                        z = -5;
                    }
                    z++;
                    y = posy +z;
                    if (n>startarana+40 and n<startarana+50 and x ==5)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+50 and x == 5)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+50 and n<startarana+60 and x ==6)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+60 and x == 6)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+60 and n<startarana+70 and x ==7)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+70 and x == 7)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+70 and n<startarana+80 and x ==8)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+80 and x == 8)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                }
                if (x>8 and x<=12)
                {
                    if (x==9)
                    {
                        z = 1;
                    }
                    z++;
                    y = posy-z;
                    if (n>startarana+80 and n<startarana+90 and x ==9)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+90 and x == 9)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+90 and n<startarana+100 and x ==10)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+100 and x == 10)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+100 and n<startarana+110 and x ==11)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+110 and x == 11)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+110 and n<startarana+120 and x ==12)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+120 and x == 12)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                }
                if (x>12 and x<=16)
                {
                    if (x==13)
                    {
                        z = -5;
                    }
                    z++;
                    y = posy +z;
                    if (n>startarana+120 and n<startarana+130 and x ==13)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+130 and x == 13)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+130 and n<startarana+140 and x ==14)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+140 and x == 14)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+140 and n<startarana+150 and x ==15)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+150 and x == 15)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+150 and n<startarana+160 and x ==16)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+160 and x == 16)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                }
                if (x>16 and x<=20)
                {
                    if (x==17)
                    {
                        z = 1;
                    }
                    z++;
                    y = posy-z;
                    if (n>startarana+160 and n<startarana+170 and x ==17)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+170 and x == 17)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+170 and n<startarana+180 and x ==18)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+180 and x == 18)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+180 and n<startarana+190 and x ==19)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+190 and x == 19)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+190 and n<startarana+200 and x ==20)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+200 and x == 20)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                }
                if (x>20 and x<=24)
                {
                    if (x==21)
                    {
                        z = -5;
                    }
                    z++;
                    y = posy +z;
                    if (n>startarana+200 and n<startarana+210 and x ==21)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+210 and x == 21)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+210 and n<startarana+220 and x ==22)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+220 and x == 22)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+220 and n<startarana+230 and x ==23)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+230 and x == 23)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+230 and n<startarana+240 and x ==24)
                    {
                        gotoxy(x, y);
                        printarana();;
                        break;
                    }
                    if (n==startarana+240 and x == 24)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                }
                if (x>24 and x<=28)
                {
                    if (x==25)
                    {
                        z = 1;
                    }
                    z++;
                    y = posy-z;
                    if (n>startarana+240 and n<startarana+250 and x ==25)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+250 and x == 25)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+250 and n<startarana+260 and x ==26)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+260 and x == 26)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+260 and n<startarana+270 and x ==27)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+270 and x == 27)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+270 and n<startarana+280 and x ==28)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+280 and x == 28)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                }
                if (x>28 and x<=30)
                {
                    if (x==29)
                    {
                        z = -5;
                    }
                    z++;
                    y = posy +z;

                    if (n>startarana+280 and n<startarana+290 and x ==29)
                    {
                        gotoxy(x, y);
                        printarana();
                        break;
                    }
                    if (n==startarana+290 and x == 29)
                    {
                        gotoxy(x, y);
                        cout<<"   ";
                        printhongos(hongos);
                    }
                    if (n>startarana+290 and n<startarana+300 and x ==30)
                    {
                        gotoxy(x,y);
                        printf("%c", 201);
                        printf("%c", 153);
                        break;
                    }
                    if (n==startarana+300 and x == 30)
                    {
                        gotoxy(x, y);
                        cout<<"  ";
                        printhongos(hongos);
                        finish = 1;
                    }
                }

            }
        }
        if (finish == 1)
        {
            this->x = 0;
            this->y = 0;
        }
    }
    return in;
}

int Arania::getx()
{
    return x;
}

void Arania::setx(int anchura)
{
    x = anchura;
}

int Arania::gety()
{
    return y;
}

void Arania::sety(int altura)
{
    y = altura;
}

int Arania::getvida()
{
    return vida;
}

void Arania::setvida(int vidas)
{
    vida = vidas;
}

int Arania::getpuntos()
{
    return puntaje;
}

void Arania::setpuntos(int puntos)
{
    puntaje = puntos;
}

void Arania::printhongos(Hongo hongos[170])
{
    for (int i=0; i<170; i++)
    {
            if(hongos[i].getx()==getx()-1 && hongos[i].gety()==gety()-1
            or hongos[i].getx()==getx()+1 && hongos[i].gety()==gety()+1
            or hongos[i].getx()==getx()+1 && hongos[i].gety()==gety()
            or hongos[i].getx()==getx()+2 && hongos[i].gety()==gety()
            or hongos[i].getx()==getx() && hongos[i].gety()==gety())
            {
                hongos[i].printhongo();
            }

            if(hongos[i].getestado() == 5) break;
    }
}

void Arania::Start(int aranastart, int aranax)
{
    startarana = aranastart;
    xarana = aranax;
}

void Arania::Restart(int aranastart, int aranax)
{
    gotoxy(x,y);
    cout<<"   ";
    x = 0;
    y = 0;
    vida = 1;
    startarana = aranastart - 200;
    if (aranax > 20)
    {
        xarana = aranax - 5;
    }
    if (aranax + 10 < 31)
    {
        xarana = aranax + 10;
    }
}

void Arania::SetLastY(int last_y)
{
    lasty = last_y;
}

int Arania::GetLastY()
{
    return(lasty);
}


