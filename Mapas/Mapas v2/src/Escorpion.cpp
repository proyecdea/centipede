#include "Escorpion.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include "windows.h"
#include "mmsystem.h"
#include "Arania.h"
#include "Hongo.h"
#include "Sonidos.h"

using namespace std;

Escorpion::Escorpion(int anchura,int altura, int rapidez,int vidas, int puntaje)
{
    x=anchura;
    y=altura;
    velocidad=rapidez;
    vida=vidas;
    puntos=puntaje;
}

Escorpion::Escorpion()
{

}

void Escorpion::color(int X)
{
SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),X);
}


void Escorpion::gotoxy(int x, int y){
	HANDLE hCon;
	hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
	dwPos.X = x;
	dwPos.Y = y;
	SetConsoleCursorPosition(hCon, dwPos);
}

int Escorpion::getx(){
    return x;
}

void Escorpion::setx(int anchura) {
    x = anchura;
}

int Escorpion::gety() {
    return y;
}

void Escorpion::sety(int altura) {
    y = altura;
}

int Escorpion::getvelocidad() {
    return velocidad;
}

void Escorpion::setvelocidad(int rapidez) {
    velocidad = rapidez;
}

int Escorpion::getvida() {
    return vida;
}

void Escorpion::setvida(int vidas) {
    vida = vidas;
}

int Escorpion::getpuntos() {
    return puntos;
}

void Escorpion::setpuntos(int puntaje) {
    puntos = puntaje;
}

Hongo Escorpion::printescorpion(int rapidez, int n, Hongo hongos[70], int colision)
{
    //Validacion de que aun no ha sido disparado
    if (colision == 4)
    {
        setx(0);
        sety(0);
        setvida(0);
    }
    else
    {
        if (n>startescorpion+1500 and getvida()>0)
        {
            startescorpion = n + 1;
            if (xescorpion + 5 < 34)
            {
                xescorpion = xescorpion + 5;
            }
        }
    }
    //Imprimir escorpion
    color(3);
    this->x = 0;
    int ex = 2;
    int w = startescorpion-10;
    int w2;
    int z;
    int imprimiendo = 0;
    if (n>=startescorpion)
    {
        if (vida>0)
        {
            this->y = xescorpion;
            while(ex<31)
            {
                w = w + 10;
                w2 = w + 10;
                z = w2 + 10;
                if (n>w and n<=w2)
                {
                    this->x = ex;
                    this->y = y;
                    gotoxy(x, y);
                    printf("%c",245);
                    imprimiendo = 1;
                }
                if (n>w2 and n<=z)
                {
                    this->x = ex;
                    this->y = y;
                    gotoxy(x, y);
                    cout<<" ";
                    imprimiendo = 1;
                }
                ex++;
                for (int i=0; i<170; i++){
                    if(hongos[i].getx()==getx() && hongos[i].gety()==gety()){
                        hongos[i].setveneno(1);
                        hongos[i].printhongo();
                    }

                    if(hongos[i].getestado() == 5) break;
                }
            }
        }
    }
    if (ex == 31 or x == 31 or imprimiendo == 0)
    {
        x = 0;
        y = 0;
    }
}

int Escorpion::soundescorpion(int rapidez, int n, Hongo hongos[70], int colision)
{
    int in=0;
    //Validacion de que aun no ha sido disparado
    if (colision == 4)
    {
        in=0;
        setx(0);
        sety(0);
        setvida(0);
    }
    else
    {
        if (n>startescorpion+1500 and getvida()>0)
        {
            startescorpion = n + 1;
            if (xescorpion + 5 < 34)
            {
                xescorpion = xescorpion + 5;
            }
        }
    }
    //Imprimir escorpion
    color(3);
    this->x = 0;
    int ex = 2;
    int w = startescorpion-10;
    int w2;
    int z;
    int imprimiendo = 0;
    if (n>=startescorpion)
    {
        if (vida>0)
        {
            this->y = xescorpion;
            while(ex<31)
            {
                w = w + 10;
                w2 = w + 10;
                z = w2 + 10;
                if (n>w and n<=w2)
                {
                    in=1;
                    this->x = ex;
                    this->y = y;
                    gotoxy(x, y);
                    printf("%c",245);
                    imprimiendo = 1;
                }
                if (n>w2 and n<=z)
                {
                    in=1;
                    this->x = ex;
                    this->y = y;
                    gotoxy(x, y);
                    cout<<" ";
                    imprimiendo = 1;
                }
                ex++;
                for (int i=0; i<170; i++){
                    if(hongos[i].getx()==getx() && hongos[i].gety()==gety()){
                        hongos[i].setveneno(1);
                        hongos[i].printhongo();
                    }

                    if(hongos[i].getestado() == 5) break;
                }
            }
        }
    }
    if (ex == 31 or x == 31 or imprimiendo == 0)
    {
        x = 0;
        y = 0;
    }
    return in;
}

void Escorpion::Start(int escorpionstart, int escorpionx)
{
    startescorpion = escorpionstart;
    xescorpion = escorpionx;
}

void Escorpion::Restart(int escorpionstart, int escorpionx)
{
    gotoxy(x,y);
    cout<<" ";
    x = 0;
    y = 0;
    vida = 1;
    startescorpion = escorpionstart - 200;
    if (escorpionx > 15)
    {
        xescorpion = escorpionx - 10;
    }
    else if (escorpionx+5 <= 20)
    {
        xescorpion = escorpionx + 5;
    }
}

