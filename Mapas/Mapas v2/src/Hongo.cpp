#include "Hongo.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include "windows.h"
#include "mmsystem.h"
using namespace std;

Hongo::Hongo(int anchura, int altura, int puntaje, int forma, int venenoso)
{
        estado=forma;
        x=anchura;
        y=altura;
        puntos=puntaje;
        veneno=venenoso;
}

Hongo::Hongo()
{

}

void Hongo::gotoxy(int x, int y){
	HANDLE hCon;
	hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
	dwPos.X = x;
	dwPos.Y = y;
	SetConsoleCursorPosition(hCon, dwPos);
}

void Hongo::color(int X){
SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),X);
}

int Hongo::getestado() {
    return estado;
}

void Hongo::setestado(int forma) {
    estado = forma;
    if (estado!=4 && estado!=0){
        puntos=5;
    }
    if (veneno==1){
        puntos=5;
    }
    else{
        puntos=0;
    }
}

int Hongo::getx(){
    return x;
}

void Hongo::setx(int anchura) {
    x = anchura;
}

int Hongo::gety() {
    return y;
}

void Hongo::sety(int altura) {
    y = altura;
}

int Hongo::getpuntos() {
    return puntos;
}

void Hongo::setpuntos(int puntaje) {
    puntos = puntaje;
}

int Hongo::getveneno() {
    return veneno;
}

void Hongo::setveneno(int venenoso) {
    veneno = venenoso;
    puntos=5;
}

void Hongo::printhongo(){

    if (veneno==0){
        color(7);
    }

    if (veneno==1){
        color(5);
    }
    switch (estado){
        case 0:
            gotoxy(x, y);
            cout<<" ";
            break;
        case 1:
            gotoxy(x, y);
            printf("%c\n",96);
            break;
        case 2:
            gotoxy(x, y);
            printf("%c\n",94);
            break;
        case 3:
            gotoxy(x, y);
            printf("%c\n",127);
            break;
        case 4:
            gotoxy(x, y);
            printf("%c\n",30);
            break;
    }
}

void Hongo::mostrarestado(){
    cout<<estado;
}
