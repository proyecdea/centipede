#ifndef HONGO_H
#define HONGO_H
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include "windows.h"
#include "mmsystem.h"
using namespace std;

class Hongo
{
    private:
        int estado;
        int x;
        int y;
        int puntos;
        int veneno;

    public:
        Hongo(int, int, int, int, int);
        Hongo();

        void gotoxy(int, int);

        void color(int);

        int getestado();
        void setestado(int);

        int getx();
        void setx(int);

        int gety();
        void sety(int);

        int getpuntos();
        void setpuntos(int);

        int getveneno();
        void setveneno(int);

        void printhongo();
        void mostrarestado();

};

#endif // HONGO_H
