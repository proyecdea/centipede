#ifndef ARANIA_H
#define ARANIA_H
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include "windows.h"
#include "mmsystem.h"
#include "Hongo.h"

using namespace std;

class Arania
{
    private:
        int x;
        int y;
        int velocidad;
        int vida = 1;
        int startarana;
        int xarana;
        int puntaje;
        int lasty;

    public:
        Arania(int, int, int, int);
        Arania();

        void gotoxy(int, int);

        void color(int);

        void printarana();

        int PrintArania(int, int, Hongo hongos[170], int);

        int getx();
        void setx(int);

        int gety();
        void sety(int);

        int getpuntos();
        void setpuntos(int);

        int getvida();
        void setvida(int);
        void printhongos(Hongo hongos[170]);

        void Start(int, int);
        void Restart(int,int);

        void SetLastY(int);
        int GetLastY();


};

#endif // ARANIA_H
