#ifndef DISPARO_H
#define DISPARO_H
#include "Hongo.h"
#include "Pulga.h"
#include "Arania.h"
#include "Escorpion.h"
#include "Centipede.h"

class Disparo
{
    private:
        int pospulga[2];
        int posarania[2];
        Hongo hongos[170];
        int limite = 0;
        int i = 0;
        int x = 99;
        int y = 0;
        int vuelta = 0;
        int distancia=0;

    public:
        Disparo(Hongo[170]);
        Disparo();
        void gotoxy(int, int);
        void color(int);
        int PosicionHongos(int, int, Hongo hongos[170]);
        int PosicionPulga(int, int, Pulga);
        int PosicionArania(int, int, Arania);
        int PosicionEscorpion(int, int, Escorpion);
        int PosicionCentipede(int, int, Centipede[12]);
        int Disparar(int, int, Hongo[170], int, Pulga, Arania, Escorpion, Centipede[12], int);
        int getdist();
        void setdist(int);
};

#endif // DISPARO_H
