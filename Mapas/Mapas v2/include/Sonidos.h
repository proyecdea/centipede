#ifndef SONIDOS_H
#define SONIDOS_H

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include "windows.h"
#include "mmsystem.h"

class Sonidos
{
    public:
        Sonidos();

        void snd_coordinar(int,int,int,int,int);

        void snd_arania(int);
        void snd_arapul(int);
        void snd_arascorp(int);
        void snd_arascorpcent(int);
        void snd_arascorppul(int);
        void snd_aracentpul(int);
        void snd_aracent(int);
        void snd_centipedesteps(int);
        void snd_centipedehit(int);
        void snd_centpul(int);
        void snd_death(int);
        void snd_disparo(int);
        void snd_enemies(int);
        void snd_win(int);
        void snd_hongos(int);
        void snd_level(int);
        void snd_pulga(int);
        void snd_scorpcent(int);
        void snd_scorpcentpul(int);
        void snd_scorpion(int);
        void snd_scorppul(int);
        void snd_menu(int);
        void snd_select(int);
};

#endif // SONIDOS_H
