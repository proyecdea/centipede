#ifndef CENTIPEDE_H
#define CENTIPEDE_H
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include "windows.h"
#include "mmsystem.h"
#include "Hongo.h"

class Centipede
{
    private:
        int x;
        int y;
        int vida;
        bool veneno;
        bool valveneno;
        int puntos;
        int direccion;
        bool cabeza;
        int antx;
        int anty;
        bool regreso;
        int posMatriz;
        bool ponHongo;

        friend class Hongo;

    public:
        Centipede(bool, int, int);
        Centipede();

        void color(int);

        void movimiento(Centipede, Hongo[], Centipede[]);
        void gotoxy(int,int);
        void dibujarCentipede();
        bool centipedeSobrepuesto(Centipede[]);

        int getx();
        void setx(int);

        int gety();
        void sety(int);

        int getVida();
        void setVida(int);

        void setdireccion(int);

        bool getcabeza();
        void setcabeza(bool);

        void setRegreso(bool);

        bool getPonHongo();
        void setPonHongo(bool);

        void Restart();

};

#endif // CENTIPEDE_H
