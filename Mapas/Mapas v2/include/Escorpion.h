#ifndef ESCORPION_H
#define ESCORPION_H

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include "windows.h"
#include "mmsystem.h"
#include "Hongo.h"

class Escorpion
{
    private:
        int x;
        int y;
        int velocidad;
        int vida = 1;
        int puntos;
        int startescorpion;
        int xescorpion;

    public:
        Escorpion(int, int, int, int, int);
        Escorpion();

        void color(int);

        void gotoxy(int,int);

        int getx();
        void setx(int);

        int gety();
        void sety(int);

        int getvelocidad();
        void setvelocidad(int);

        int getvida();
        void setvida(int);

        Hongo printescorpion(int, int, Hongo[170], int);
        int soundescorpion(int, int, Hongo[170], int);

        int getpuntos();
        void setpuntos(int);

        void Start(int, int);
        void Restart(int, int);

};

#endif // ESCORPION_H
