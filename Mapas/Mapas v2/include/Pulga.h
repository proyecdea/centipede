#ifndef PULGA_H
#define PULGA_H

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <time.h>
#include "windows.h"
#include "mmsystem.h"
#include "Hongo.h"

using namespace std;

class Pulga
{
    private:
        int x;
        int y;
        int velocidad;
        int vida = 1;
        int puntos;
        int startpulga;
        int xpulga;

    public:
        Pulga(int, int, int, int, int);
        Pulga();

        void color(int);

        void gotoxy(int,int);

        int getx();
        void setx(int);

        int gety();
        void sety(int);

        int getvelocidad();
        void setvelocidad(int);

        int getvida();
        void setvida(int);

        int printpulga(int, int, Hongo[170], int);

        int getpuntos();
        void setpuntos(int);

        void Start(int, int);

        void Restart(int, int);

};

#endif // PULGA_H
