#ifndef JUGADOR_H
#define JUGADOR_H
#include "Disparo.h"
#include "Hongo.h"
#include "Pulga.h"
#include "Arania.h"
#include "Escorpion.h"
#include "Sonidos.h"


class Jugador
{
    private:
        int x = 16;
        int y = 32;
        int vidas = 3;
        Disparo disparo;

    public:
        Jugador(int, int, int);
        Jugador();
        void gotoxy(int, int);
        void color(int);
        void ValidacionContacto(int, int, Pulga, Arania, Escorpion, Centipede[12]);
        int Movimiento(Disparo, Hongo[170], int, int[2], Pulga, Arania, Escorpion, Centipede[12], int);
        int getvidas();
        void setx(int);
        void sety(int);
        void Restart();

};

#endif // JUGADOR_H
